# Ikhnos

This project gets waterfall from a given observation and applies an overlay of the signal generated from given TLEs.

# Instructions

Install requirements (better in a virtual environment):

```
pip -r requirements
```

Run the srcipt:

```
python ikhnos.py <satnogs_observation_number> <filename_of_tles_file>
```
If you have a file named "tle" with the TLEs then you can omit the second argument.

TLEs should contain 3 lines.

# Format of the output filenname

<Datetime>_<observation_number>_<TLE_second_line_until_epoch>_<How_long_in_day_fraction_TLE_issued_from_start_time>_<TLE_issued_(A)fter_or_(B)efore_the_start_time>

Example:

2018-11-29T13:05:40_342795_43737U 18096_0.0985139281481_A

# Acknowledgement

Thanks to cgbsat for the initial script code.

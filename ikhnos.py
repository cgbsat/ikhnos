#!/usr/bin/env python
import sys
from PIL import Image
import ephem
import datetime
import matplotlib.pyplot as plt
import numpy as np
import requests
import json
import shutil
from mutagen.oggvorbis import OggVorbis
import re
import os.path

plt.switch_backend('Agg')

print(sys.argv[1])

try:
    obs_id = sys.argv[1]
except:
    print('Please give a valid observation id')
    sys.exit()

print("Requesting observation " + obs_id)
r = requests.get('https://network.satnogs.org/api/observations/' + obs_id + '/?format=json')
observation = json.loads(r.content.decode('utf8'))
print(observation)


print("Downloading the observation page for getting frequency and tle")
observation_link = "https://network.satnogs.org/observations/" + obs_id
obs_html = requests.get(observation_link).content
tle_regex = r"<pre>(1 .*)<br>(2 .*)</pre>"
tle_matches = re.search(tle_regex, obs_html.decode("utf-8"))
freq_regex = r"(\d*\.\d*) MHz"
freq_matches = re.search(freq_regex, obs_html.decode("utf-8"))

if not os.path.isfile(obs_id + '.ogg'):
    print("Downloading " + observation['payload'] + ' for getting the right duration')
    observation_ogg_data = requests.get(observation['payload'], stream=True).content
    with open(obs_id + '.ogg', 'wb') as out_file:
        out_file.write(observation_ogg_data)

if not os.path.isfile(obs_id + '.png'):
    print("Downloading " + observation['waterfall'] + ' for the background image')
    img_data = requests.get(observation['waterfall']).content
    with open(obs_id + '.png', 'wb') as handler:
        handler.write(img_data)

# Set tle
tle0 = "Estimated TLE"
tle1 = tle_matches.group(1)
tle2 = tle_matches.group(2)
satellite1 = ephem.readtle(tle0, tle1, tle2)

observer = ephem.Observer()
observer.lat = str(observation['station_lat'])
observer.lon = str(observation['station_lng'])
observer.elevation = observation['station_alt']
freq0 = float(freq_matches.group(1))
tstart = observation['start'].replace('Z', '')
start = datetime.datetime.strptime(tstart, "%Y-%m-%dT%H:%M:%S")
ystart = datetime.datetime(start.year, 1, 1)
diff = start-ystart
#+1 day in timedelta as TLE show the day and a fraction of it so Jan 1st is 1 not 0.
obs_epoch_day = ((start - ystart).total_seconds() + datetime.timedelta(days=1).total_seconds()) / datetime.timedelta(days=1).total_seconds()
f = OggVorbis(obs_id + '.ogg')
nseconds = int(round(f.info.length))

tle_file = 'tle'
if len(sys.argv) > 2:
    tle_file = sys.argv[2]
with open(tle_file) as f:
    tle_lines = f.read().splitlines()
tles = []
for i in range(0, len(tle_lines), 3):
     tles.append(tle_lines[i:i + 3])

for tba_tle in tles:

    satellite2 = ephem.readtle(tba_tle[0], tba_tle[1], tba_tle[2])
    epoch_regex = r".{20}\s*(\d*\.\d*)"
    epoch_matches = re.search(epoch_regex, tba_tle[1])
    epoch_day = epoch_matches.group(1)
    obs_tle_epochs_diff = float(epoch_day) - obs_epoch_day
    if abs(obs_tle_epochs_diff) > 2:
        continue

    times = [start+datetime.timedelta(seconds=s) for s in range(0, nseconds)]

    v1 = []
    v2 = []
    for t in times:
        observer.date = t
        satellite1.compute(observer)
        satellite2.compute(observer)
        v1.append(satellite1.range_velocity)
        v2.append(satellite2.range_velocity)

    freq1 = freq0*(1.0-np.array(v1)/299792458.0)
    freq2 = freq0*(1.0-np.array(v2)/299792458.0)
    dfreq = (freq2-freq1)*1000.0

    t = np.arange(nseconds)

    fname_overlay = "%05d_%s_overlay.png" % (satellite2.catalog_number, obs_id)
    
    plt.figure(figsize=(10 * 0.8,20))
    plt.plot(dfreq, t, color='red', linewidth=0.5)
    plt.ylabel("Time (seconds)")
    plt.xlabel("Frequency (kHz)")
    plt.xlim(-24.0, 24.0)
    plt.ylim(0, nseconds)
    plt.savefig(fname_overlay, bbox_inches="tight", transparent=True)
    plt.close()
    background = Image.open(obs_id + ".png")
    overlay = Image.open(fname_overlay)

    background = background.convert("RGBA")
    overlay = overlay.convert("RGBA")

    ov = Image.new('RGBA',background.size, (0, 0, 0, 0))
    ov.paste(overlay, overlay.getbbox())
    background.paste(ov, (0,0), ov)
    if not os.path.exists(obs_id):
        os.makedirs(obs_id)
    fname_png = os.path.join("%s"%obs_id, "%05d_%s_%.3f.png" % (satellite2.catalog_number, obs_id, obs_tle_epochs_diff))
    background.save(fname_png, "PNG")
    background.close()
    overlay.close()
    os.remove(fname_overlay)
